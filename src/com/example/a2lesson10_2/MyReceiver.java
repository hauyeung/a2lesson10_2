package com.example.a2lesson10_2;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.widget.Toast;

public class MyReceiver extends BroadcastReceiver {

	@Override
	public void onReceive(Context context, Intent intent) {
		// TODO Auto-generated method stub
		Bundle b = intent.getExtras();
		String message = b.getString("message");
		Toast.makeText(context, "Received a message: "+message, Toast.LENGTH_LONG).show();

	}

}
